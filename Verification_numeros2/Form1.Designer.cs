﻿namespace Verification_numeros2
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_select_docx = new System.Windows.Forms.Button();
            this.btn_select_pdf = new System.Windows.Forms.Button();
            this.txtbox_docx = new System.Windows.Forms.TextBox();
            this.txtbox_pdf = new System.Windows.Forms.TextBox();
            this.btn_start = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_select_docx
            // 
            this.btn_select_docx.Location = new System.Drawing.Point(34, 28);
            this.btn_select_docx.Name = "btn_select_docx";
            this.btn_select_docx.Size = new System.Drawing.Size(104, 23);
            this.btn_select_docx.TabIndex = 1;
            this.btn_select_docx.Text = "Select Docx file";
            this.btn_select_docx.UseVisualStyleBackColor = true;
            this.btn_select_docx.Click += new System.EventHandler(this.btn_select_docx_Click);
            // 
            // btn_select_pdf
            // 
            this.btn_select_pdf.Location = new System.Drawing.Point(34, 74);
            this.btn_select_pdf.Name = "btn_select_pdf";
            this.btn_select_pdf.Size = new System.Drawing.Size(104, 23);
            this.btn_select_pdf.TabIndex = 2;
            this.btn_select_pdf.Text = "Select PDF file";
            this.btn_select_pdf.UseVisualStyleBackColor = true;
            this.btn_select_pdf.Click += new System.EventHandler(this.btn_select_pdf_Click);
            // 
            // txtbox_docx
            // 
            this.txtbox_docx.Location = new System.Drawing.Point(182, 28);
            this.txtbox_docx.Name = "txtbox_docx";
            this.txtbox_docx.Size = new System.Drawing.Size(100, 20);
            this.txtbox_docx.TabIndex = 3;
            // 
            // txtbox_pdf
            // 
            this.txtbox_pdf.Location = new System.Drawing.Point(182, 77);
            this.txtbox_pdf.Name = "txtbox_pdf";
            this.txtbox_pdf.Size = new System.Drawing.Size(100, 20);
            this.txtbox_pdf.TabIndex = 4;
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(117, 132);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 23);
            this.btn_start.TabIndex = 5;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 184);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.txtbox_pdf);
            this.Controls.Add(this.txtbox_docx);
            this.Controls.Add(this.btn_select_pdf);
            this.Controls.Add(this.btn_select_docx);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_select_docx;
        private System.Windows.Forms.Button btn_select_pdf;
        private System.Windows.Forms.TextBox txtbox_docx;
        private System.Windows.Forms.TextBox txtbox_pdf;
        private System.Windows.Forms.Button btn_start;
    }
}

