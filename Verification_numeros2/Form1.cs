﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;


using java.util;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;
using Console = System.Console;

using Word = Microsoft.Office.Interop.Word;
using jdk.@internal.org.objectweb.asm.tree;
using System.Collections;

namespace Verification_numeros2
{
    
    public partial class Form1 : Form
    {

        string path_docx = "";
        string path_pdf = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_select_docx_Click(object sender, EventArgs e)
        {

            OpenFileDialog ofd_docx = new OpenFileDialog();
            ofd_docx.Filter = "Docx files (*.docx)|*.docx|All files (*.*)|*.*";
            ofd_docx.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if (ofd_docx.ShowDialog() == DialogResult.OK)
            {

                path_docx = ofd_docx.FileName;
                txtbox_docx.Text = path_docx;

            }

        }

        private void btn_select_pdf_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd_pdf = new OpenFileDialog();
            ofd_pdf.Filter = "PDF files (*.pdf)|*.pdf|All files (*.*)|*.*";
            ofd_pdf.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if (ofd_pdf.ShowDialog() == DialogResult.OK)
            {

                path_pdf = ofd_pdf.FileName;
                txtbox_pdf.Text = path_pdf;

            }
        }

        private void btn_start_Click(object sender, EventArgs e)
        {

            var txtPDF_path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Output_PDF.txt");
            var txtDOCX_path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Output_DOCX.txt");
            DocPDF doc_pdf = new DocPDF(path_pdf);
            DocDOCX doc_docx = new DocDOCX(path_docx);
                    
            string output_pdf_str = string.Join(Environment.NewLine, doc_pdf.GetTextList());
            System.IO.File.WriteAllText(txtPDF_path, output_pdf_str);


            string test1 = doc_docx.GetText();
            //List<string[]> test2 = doc_docx.GetPOSTag(test1);

            List<string[]> output_POS_list = doc_docx.GetPOSTag(doc_docx.GetText()); //On applique la méthode GetPOSTag au string issu de la méthode GetText de la classe DocDOCX

            List<string[]> output_DOCX_list = doc_docx.GetCoupleGroupeNominalEtNumero(output_POS_list);

            System.IO.File.WriteAllLines(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Output_DOCX_list.txt"), output_DOCX_list.SelectMany(line => line) , Encoding.UTF8);
            System.IO.File.WriteAllLines(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Output_POS_list.txt"), output_POS_list.SelectMany(line => line), Encoding.UTF8);


            MessageBox.Show("text written");

        }
    }

    public class DocPDF
    {
        public string _path;
        public DocPDF(string path)
        {
            this._path = path;
        }

        public List<string> GetTextList()
        {
            List<string> output_list = new List<string>();
            using (PdfReader reader = new PdfReader(_path))
            {
                StringBuilder text = new StringBuilder();

                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }

                String[] text_array = text.ToString().Split('\n');
                List<string> text_list = new List<string>();

                foreach (string element in text_array)
                {
                    if (!(element.Contains('/')) && !(element.ToUpper().Contains("FIG")) && element.Trim() != "")
                    {
                        text_list.Add(element.Trim());
                    }

                }


                foreach (string element in text_list)
                {
                    var sub_array = element.Split(new[] { ' ', ',' });

                    foreach (string subelement in sub_array)
                    {
                        output_list.Add(subelement.Trim());
                    }

                }

                output_list = output_list.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();

                            }

            return output_list;
        }

    }

    public class DocDOCX
    {
        public string _path;
        public DocDOCX (string path)
        {
            this._path = path;
        }

        
        public string GetText()
        {
            StringBuilder text = new StringBuilder();
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            object miss = System.Reflection.Missing.Value;
            object path2 = System.IO.Path.Combine(_path, "");
            object readOnly = true;
            Microsoft.Office.Interop.Word.Document docs = word.Documents.Open(ref path2, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);

            for (int i = 0; i < docs.Paragraphs.Count; i++)
            {
                text.Append(" \r\n " + docs.Paragraphs[i + 1].Range.Text.ToString());
            }

            return text.ToString();
        }

        public List<string[]> GetPOSTag (string text)
        {
            List<string[]> output_tag_list = new List<string[]>();

            var jarRoot = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "standfordtagger4"); ;
            var modelsDirectory = jarRoot + @"\models";
            var tagger = new MaxentTagger(modelsDirectory + @"\french-ud.tagger"); // Loading POS Tagger

            var sentences = MaxentTagger.tokenizeText(new java.io.StringReader(text)).toArray(); // IL FAUDRAIT SEPARER L' et D' DU NOM OU DU VERBE QUI SUIT - REVOIR LA TOKENIZATION

            foreach (java.util.ArrayList sentence in sentences)
            {
                var taggedSentence = tagger.tagSentence(sentence);
                foreach (var element in taggedSentence.toArray())
                {
                    string element2 = element.ToString(); // element2 a le format "12/NUM"
                    var element2_TAG = element2.Substring(element2.LastIndexOf('/') + 1); //On sépare "12/NUM" en [12, NUM] - On prend le dernier "/" pour séparer
                    var element2_TEXT = element2.Substring(0, element2.LastIndexOf('/'));
                    string[] element2_array = new string[] { element2_TEXT, element2_TAG };
                    output_tag_list.Add(element2_array);

                }
            }

                return output_tag_list;
        }

        public Object CheckIfNum (int i, List<string[]> POS_list) 
        {
            bool isNum = false;
            int indiceNumStart = 0;
            int indiceNumEnd = 0;
            //List<string[]> test = POS_list;
            if (POS_list[i][1] == "NUM" && 
            (POS_list[i - 1][1] == "NOUN" ||
            (POS_list[i - 2][1] == "NOUN" && POS_list[i - 1][1] == "ADJ") ||
            (POS_list[i - 2][1] == "NOUN" && POS_list[i - 1][1] == "VERB") ||
            (POS_list[i - 2][1] == "PROPN" && POS_list[i - 1][1] == "VERB") ||
            (POS_list[i - 2][1] == "NOUN" && POS_list[i - 1][1] == "PROPN") ||
            (POS_list[i - 2][1] == "NOUN" && POS_list[i - 1][1] == "NOUN") ||
            (POS_list[i - 3][1] == "NOUN" && POS_list[i - 2][1] == "ADJ" && POS_list[i - 1][1] == "ADJ") ||
            (POS_list[i - 3][1] == "NOUN" && POS_list[i - 2][1] == "VERB" && POS_list[i - 1][1] == "ADJ") ||
            (POS_list[i - 3][1] == "NOUN" && POS_list[i - 2][1] == "ADJ" && POS_list[i - 1][1] == "VERB") ||
            (POS_list[i - 3][1] == "NOUN" && POS_list[i - 2][1] == "ADV" && POS_list[i - 1][1] == "ADJ")))
            {
                isNum = true;
                indiceNumStart = i;
                indiceNumEnd = i;
                if (POS_list[i + 2][1] == "NUM")
                { indiceNumEnd = i + 2; }
            }

           
            Object[] output_array = new Object[] { isNum, indiceNumStart, indiceNumEnd };

            return output_array;
            

        }
    
        public string GetGroupeNominal(int index, List<string[]> POS_list)
        {
            string output = "";
            bool determinant_found = false;
            int start_index = index;
            int i = start_index;

            while (determinant_found == false)
            {
                if (POS_list[i][1] != "DET" && POS_list[i][1] != "VERB" && POS_list[i][1] != "PRON" && POS_list[i][1] != "PROPN")
                {
                    i--;
 
                }
                else
                {
                    determinant_found = true;
                    int end_index = i;

                    foreach (string[] element in POS_list.GetRange(end_index, start_index-end_index))
                    { 
                        output += element[0] + ' '; 
                    }
                    
                }
            }


            return output;

        }

        public List<string[]> RemoveDuplicates(List<string[]> list_with_duplicates)
        {
            List<string[]> list_without_duplicates = new List<string[]>();
            foreach (string[] element in list_with_duplicates)
            { 
                //A TROUVER
            }

            return list_without_duplicates;
        }

        public List<string[]> GetCoupleGroupeNominalEtNumero(List<string[]> POSTag_list)
        {
            List<string[]> output_docx_ref_listing = new List<string[]>();

            for (int i=0 ; i < POSTag_list.Count(); i++)
            {
                string output_num_string = "";
                string output_groupe_nominal_string = "";
                

                object obj = CheckIfNum(i, POSTag_list);
                string[] CheckIfNum_arr = ((IEnumerable)obj).Cast<object>().Select(x => x.ToString()).ToArray(); //on transforme l'objet CheckIfNum(i, POSTag_list) en array

                if (CheckIfNum_arr[0].ToLower().ToString() == "true") //c'est bien moche mais je n'arrive pas accéder à l'index 0 d'un objet
                {
                    int start_index = int.Parse(CheckIfNum_arr[1]);
                    int end_index = int.Parse(CheckIfNum_arr[2]);
                    for (int j=start_index ; j < (end_index+1) ; j++)
                    {
                        output_num_string = output_num_string + POSTag_list[j][0] + ' ';
                    }

                    output_groupe_nominal_string = GetGroupeNominal(i, POSTag_list);

                    if (!output_groupe_nominal_string.ToLower().Contains("revendication") && !output_groupe_nominal_string.ToLower().Contains("figure"))
                    {

                    string[] temp_array = new string[2] { output_groupe_nominal_string.ToLower(), output_num_string};
                    output_docx_ref_listing.Add(temp_array);
                    output_docx_ref_listing = output_docx_ref_listing.OrderBy(r => r[1]).ToList(); //Ordonner la liste par numéro de référence
                    output_docx_ref_listing = output_docx_ref_listing.Union(output_docx_ref_listing).ToList(); //Supprimer les doublons dans la liste - NE FONCTIONNE PAS

                    }


        }


            }

            return output_docx_ref_listing;

        }
        
    }

    
}
